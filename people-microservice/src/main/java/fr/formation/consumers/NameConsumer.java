package fr.formation.consumers;

import org.eclipse.microprofile.reactive.messaging.Incoming;

import io.smallrye.reactive.messaging.kafka.Record;
import jakarta.enterprise.context.ApplicationScoped;
import lombok.extern.java.Log;

@ApplicationScoped
@Log
public class NameConsumer {
    
    @Incoming("names")
    public void consume(Record<Long, String> message) {
        log.info(message.key() + " " + message.value());
    }

}
