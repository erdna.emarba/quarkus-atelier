package fr.formation.models;

import jakarta.persistence.Entity;
import jakarta.persistence.GeneratedValue;
import jakarta.persistence.Id;
import lombok.Data;

@Data
@Entity
public class Person {
    
    @Id
    @GeneratedValue
    private long id;

    private String name;
}
