package fr.formation.repositories;

import fr.formation.models.Person;
import io.quarkus.hibernate.reactive.panache.PanacheRepository;
import io.smallrye.mutiny.Uni;
import jakarta.enterprise.context.ApplicationScoped;

@ApplicationScoped
public class PersonRepository implements PanacheRepository<Person> {

    public Uni<Person> update(Person person) {
        return getSession().onItem().transformToUni(session -> session.merge(person));
    }

}
