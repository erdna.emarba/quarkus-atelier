package fr.formation.resources;

import java.util.List;

import org.jboss.resteasy.reactive.RestPath;
import org.jboss.resteasy.reactive.RestResponse;
import org.jboss.resteasy.reactive.RestResponse.Status;

import fr.formation.models.Person;
import fr.formation.repositories.PersonRepository;
import io.quarkus.hibernate.reactive.panache.common.WithTransaction;
import io.smallrye.mutiny.Uni;
import jakarta.ws.rs.DELETE;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import lombok.AllArgsConstructor;

@AllArgsConstructor
@Path("people")
public class PersonResource {

    PersonRepository personRepository;
    
    @GET
    public Uni<List<Person>> findAll() {
        return personRepository.listAll();
        
    }

    @GET
    @Path("{id}")
    public Uni<Person> findById(@PathParam("id") long id) {
        return personRepository.findById(id)
            .onItem().ifNull().failWith(() -> new NotFoundException("no person with id " + id));
    }

    @POST
    @WithTransaction
    public Uni<Person> save(Person person) {
        return personRepository.persist(person);
    }

    @PUT
    @Path("{id}")
    @WithTransaction
    public Uni<RestResponse<Void>> update(@PathParam("id") long id, Person person) {
        if (person.getId() == 0)
            person.setId(id);
        else if (person.getId() != id)
            return Uni.createFrom().item(RestResponse.status(Status.BAD_REQUEST));
        return personRepository.findById(id)
            .onItem().ifNull().failWith(() -> new NotFoundException("no person with id " + id))
            .onItem().transformToUni(p -> personRepository.update(person))
            .onItem().transform(p -> RestResponse.ok());
    }

    @DELETE
    @Path("{id}")
    @WithTransaction
    public Uni<RestResponse<Void>> delete(@PathParam("id") long id) {
        return personRepository.deleteById(id)
            .onItem().transform(deleted -> (deleted ? RestResponse.ok() : RestResponse.status(Status.NOT_FOUND)));
    }

}
