package org.acme.services;

import org.eclipse.microprofile.rest.client.inject.RegisterRestClient;

import io.smallrye.mutiny.Uni;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.PUT;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import lombok.Getter;
import lombok.Setter;

@Path("people")
@RegisterRestClient
public interface PeopleService {

    @Getter
    @Setter
    public static class Person {
        private long id;
        private String name;
    }

    @Path("{id}")
    @GET
    Uni<Person> findById(@PathParam("id") long id);

    @Path("{id}")
    @PUT
    Uni<Void> update(@PathParam("id") long id, Person person);
}
