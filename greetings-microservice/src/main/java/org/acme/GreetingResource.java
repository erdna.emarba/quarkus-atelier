package org.acme;

import org.acme.services.PeopleService;
import org.eclipse.microprofile.reactive.messaging.Channel;
import org.eclipse.microprofile.reactive.messaging.Emitter;
import org.eclipse.microprofile.rest.client.inject.RestClient;

import io.smallrye.mutiny.Uni;
import jakarta.inject.Inject;
import jakarta.ws.rs.GET;
import jakarta.ws.rs.NotFoundException;
import jakarta.ws.rs.POST;
import jakarta.ws.rs.Path;
import jakarta.ws.rs.PathParam;
import jakarta.ws.rs.Produces;
import jakarta.ws.rs.QueryParam;
import jakarta.ws.rs.core.MediaType;
import io.smallrye.reactive.messaging.kafka.Record;


@Path("/")
public class GreetingResource {

    @Inject
    @RestClient
    PeopleService peopleService;

    @Inject
    @Channel("names")
    Emitter<Record<Long, String>> namesEmitter;

    @GET
    @Path("hello/{id}")
    @Produces(MediaType.TEXT_PLAIN)
    public Uni<String> hello(@PathParam("id") long id) {
        return peopleService.findById(id)
            .onFailure().transform(NotFoundException::new)
            .onItem().transform(p -> "Hello " + p.getName() + " !");
    }

    @POST
    @Path("/upper/{id}")
    public Uni<String> upper(@PathParam("id") long id) {
        return peopleService.findById(id)
            // .onFailure().transform(NotFoundException::new)
            .onItem().transform(p -> {
                p.setName(p.getName().toUpperCase());
                return p;
            })
            .onItem().transformToUni(p -> peopleService.update(p.getId(), p))
            .onItem().transform(p -> "done !");
    }

    @POST
    @Path("/rename/{id}")
    public Uni<Void> rename(
        @PathParam("id") long id,
        @QueryParam("name") String name
    ) {
        return Uni.createFrom().completionStage(namesEmitter.send(Record.of(id, name)));
    }
}
